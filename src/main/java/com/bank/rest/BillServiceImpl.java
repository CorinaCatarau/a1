package com.bank.rest;

import com.bank.dao.BillDAO;
import com.bank.model.Bill;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Corina on 02.04.2016.
 */
@Service
public class BillServiceImpl implements BillService {

    @Autowired
    private BillDAO billDAO;


    @Override
    public Bill create(Bill bill) {
        return billDAO.create(bill);
    }

    @Override
    public void delete(int id) {
        billDAO.delete(id,Bill.class);
    }

    @Override
    public void update(Bill bill) {billDAO.update(bill);}

    @Override
    public Bill getBill(int id) {
        return billDAO.get(id,Bill.class);
    }

    @Override
    public List<Bill> getAll() {
        return this.billDAO.getAll();
    }

    @Override
    public List<Bill> getAllOfClient(int id) {return billDAO.getAllOfClient(id);}


}
