package com.bank.rest;

import com.bank.model.Account;
import com.bank.model.Client;
import com.bank.model.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import java.util.List;

/**
 * Created by Corina on 01.04.2016.
 */
@Controller
@EnableWebMvc
@RequestMapping("/rest")
public class AccountServiceController {

    @Autowired
    private AccountService accountService;
    @Autowired
    private  ClientService clientService;

    @RequestMapping(value = "{clientId}/account", method = RequestMethod.POST,
            consumes ="application/JSON",
            produces = "application/JSON")
    @ResponseBody
    public ResponseEntity<Account> createAccount(@PathVariable int clientId,@RequestBody Account account) {
        int id=0;
        Client client=clientService.getClient(clientId);
        if(account.isBillAccount()) {
            if (accountService.hasBillAccount(clientId)) {
                return new ResponseEntity<Account>(HttpStatus.FORBIDDEN);
            }
        }
        if(client!=null) {
            try {
                if(account.getAmount()<=0){
                    return new ResponseEntity<Account>(HttpStatus.NOT_ACCEPTABLE);
                }else if((account.getName()!=null)) {
                    account.setClient(client);
                    id = accountService.create(account).getId();
                    client.getAccountList().add(accountService.getAccount(id));
                }else{
                    return new ResponseEntity<Account>(HttpStatus.BAD_REQUEST);
                }

                } catch (Exception e) {
                  return new ResponseEntity<Account>(HttpStatus.NOT_FOUND);
                }
                    HttpHeaders header = new HttpHeaders();
                    header.add("Location", "/rest/accounts/" + String.valueOf(id));
                    return new ResponseEntity<Account>(account, header, HttpStatus.CREATED);
                }
                return new ResponseEntity<Account>(HttpStatus.NOT_FOUND);

            }






    @RequestMapping(value = "/accounts/{idAccount}", method = RequestMethod.GET, produces ="application/JSON")
    @ResponseBody
    public ResponseEntity<Account> getAccount(@PathVariable int idAccount) {
        try {
            Account account = accountService.getAccount(idAccount);
            return new ResponseEntity<>(account, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }

    @RequestMapping(value = "/accounts", method = RequestMethod.GET,produces = "application/JSON")
    @ResponseBody
    public  ResponseEntity<List<Account>> getAll(Model model) {
        List<Account> accounts;
        try {
            model.addAttribute("account", new Account());
            model.addAttribute("listAccounts", this.accountService.getAll());
            accounts= this.accountService.getAll();
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(accounts, HttpStatus.OK);
    }

    @RequestMapping(value = "/accounts/clients/{idClient}", method = RequestMethod.GET,produces = "application/JSON")
    @ResponseBody
    public  ResponseEntity<List<Account>> getAllOfClient(@PathVariable("idClient") int idClient,Model model) {
        List<Account> accounts;
        try {
            model.addAttribute("account", new Account());
            model.addAttribute("listAccountsOfClient", this.accountService.getAllOfClient(idClient));
            accounts= this.accountService.getAllOfClient(idClient);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(accounts, HttpStatus.OK);
    }

    @RequestMapping(value = "/accounts/{idAccount}", method = RequestMethod.PUT)
    @ResponseBody
    public ResponseEntity<Account> updateAccount(@PathVariable("idAccount") int idAccount,  @RequestBody  Account account) {
        try {
            if(account.getAmount()<=0){
                return new ResponseEntity<Account>(HttpStatus.NOT_ACCEPTABLE);
            } else if((account.getName()!=null)) {
                account.setId(idAccount);
                accountService.update(account);
            }else{
                return new ResponseEntity<Account>(HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            return new ResponseEntity<Account>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<Account>(account, HttpStatus.OK);
    }



    @RequestMapping(value = "/accounts/{idAccount}", method = RequestMethod.DELETE)
    @ResponseBody
    public ResponseEntity<Account> deleteAccount(@PathVariable("idAccount") int idAccount) {
        try {
            accountService.delete(idAccount);
        } catch (Exception e) {
            return new ResponseEntity<Account>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<Account>(HttpStatus.OK);
    }

    @RequestMapping(value = "/transaction", method = RequestMethod.POST,
            consumes ="application/JSON",
            produces = "application/JSON")
    @ResponseBody
    public boolean createTransaction(@RequestBody Transaction transaction) {

        if (accountService.withdrawMoneyFromAccount(transaction.getSourceAccountId(), transaction.getAmount()) == true) {
            if (accountService.depositMoneyToAccount(transaction.getDestinationAccountId(), transaction.getAmount()) == true) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }
}
