package com.bank.rest;

import com.bank.dao.ActivityDAO;
import com.bank.model.Activity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Corina on 02.04.2016.
 */
@Service
public class ActivityServiceImpl implements ActivityService {

    @Autowired
    private ActivityDAO activityDAO;


    @Override
    public Activity create(Activity activity) {
        return activityDAO.create(activity);
    }

    @Override
    public void delete(int id) {
        activityDAO.delete(id,Activity.class);
    }

    @Override
    public void update(Activity activity) {activityDAO.update(activity);}

    @Override
    public Activity getActivity(int id) {
        return activityDAO.get(id,Activity.class);
    }

    @Override
    public List<Activity> getAll() {
        return this.activityDAO.getAll();
    }

    @Override
    public List<Activity> getAllOfUser(int id) {return activityDAO.getAllOfUser(id);}

    @Override
    public List<Activity> getReportActivities(int userId, String startDate, String endDate) {return activityDAO.getReportActivities(userId,startDate,endDate);}

}

