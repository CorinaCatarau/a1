package com.bank.rest;

import com.bank.model.Bill;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by Corina on 02.04.2016.
 */
@Component
public interface BillService {

    public Bill create(Bill bill);
    public void delete(int id);
    public void update(Bill bill);
    public Bill getBill(int id);
    public List<Bill> getAll();
    public List<Bill> getAllOfClient(int id);


}
