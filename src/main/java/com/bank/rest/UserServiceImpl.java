package com.bank.rest;

import com.bank.dao.UserDAO;
import com.bank.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Corina on 30.03.2016.
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDAO userDAO;

    @Override
    public User create(User user) {
        return userDAO.create(user);
    }

    @Override
    public void delete(int id) {
        userDAO.delete(id,User.class);
    }

    @Override
    public void update(User user) {userDAO.update(user);}

    @Override
    public User getUser(int id) {
        return userDAO.get(id,User.class);
    }

    @Override
    public List<User> getAll() {
        return this.userDAO.getAll();
    }

    @Override
    public User findByUsername(String username) {return this.userDAO.findByUserName(username);}
}
