package com.bank.rest;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by Corina on 04.04.2016.
 */
@Controller
@RequestMapping("/")
public class UiController {

    public static final String PATH = "common";
    public static final String USER = "[USER_ROLE]";
    public static final String ADMIN = "[ADMIN_ROLE]";
    public static final String USEREVALUATION = "user-view-assigned-evaluations";
    public static final String TESTS = "tests";

    @RequestMapping(value = "/{path}", method = RequestMethod.GET)
    public String getPathPage(@PathVariable("path") String path) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
                return path;

    }


    @RequestMapping(value = "/admin/{path}", method = RequestMethod.GET)
    public String getAdminPathPage(@PathVariable("path") String path) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if(ADMIN.equals(auth.getAuthorities().toString())){
            return path;
        }
        return "/login";

    }
}
