package com.bank.rest;

import com.bank.model.Client;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by Corina on 01.04.2016.
 */
@Component
public interface ClientService {

        public Client create(Client client);
        public void delete(int id);
        public void update(Client client);
        public Client getClient(int id);
        public List<Client> getAll();
        public List<Client> getByCnp(String cnp);
        public boolean validateCNP(String cnp );


}
