package com.bank.dao;

import com.bank.model.Activity;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by Corina on 02.04.2016.
 */
@Component
public interface ActivityDAO extends GenericDAO<Activity,Integer> {
    public List getAll();
    public  List getAllOfUser(int userid);
    public  List getReportActivities(int id,String startDate,String endDate);


}