package com.bank.dao;

import com.bank.model.User;
import com.bank.model.UserRole;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Set;

/**
 * Created by Corina on 28.03.2016.
 */
@Component
public interface UserDAO extends GenericDAO<User,Integer> {
        public List getAll();
        User findByUserName(String username);

}

