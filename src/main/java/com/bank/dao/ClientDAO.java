package com.bank.dao;

import com.bank.model.Client;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by Corina on 01.04.2016.
 */
@Component
public interface ClientDAO extends GenericDAO<Client,Integer> {
    public List getAll();
    public List<Client> getByCnp(String cnp);
}
