package com.bank.dao;

import com.sun.corba.se.spi.ior.Identifiable;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Corina on 31.03.2016.
 */

public class GenericDAOImpl<T,K> implements GenericDAO<T,K>{
    @Autowired
    private SessionFactory sessionFactory;


    @Override
    public T create(T object) {
        Session session=this.sessionFactory.getCurrentSession();
        Transaction tx= session.beginTransaction();
        session.clear();
        session.persist(object);
        session.flush(); //force the SQL INSERT
        tx.commit();
        return object;
    }

    @Override
    public void delete(int id,Class<T> type) {
        Session session=this.sessionFactory.getCurrentSession();
        Transaction tx= session.beginTransaction();
        Object persistentInstance = session.get(type, id);
        if (persistentInstance != null) {
            session.delete(persistentInstance);
            session.flush(); //force the SQL INSERT

        }
        tx.commit();


    }

    @Override
    public T update(T object) {
        Session session=this.sessionFactory.getCurrentSession();
       Transaction tx= session.beginTransaction();
        session.clear();
        T updateObject=(T)session.merge(object);
        session.flush(); //force the SQL INSERT
        tx.commit();
        tx.rollback();
        return updateObject;
    }

    @Override
    public T get(int id,Class<T> type) {
        Session session=this.sessionFactory.getCurrentSession();
        Transaction tx= session.beginTransaction();
        T object=(T) session.get(type,id);
        tx.commit();
        return object;
    }


}
