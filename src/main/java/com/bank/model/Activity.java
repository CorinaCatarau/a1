package com.bank.model;

import javax.persistence.*;

/**
 * Created by Corina on 02.04.2016.
 */
@Entity
@Table(name="activity")
public class Activity {
    @Id
    @GeneratedValue
    @Column(name="id")
    private int id;

    @Column(name="name")
    private String name;

    @Column(name="date")
    private String date;

    @ManyToOne
    private User user;

    public int getId() {return id;}

    public void setId(int id) {this.id = id;}

    public String getName() {return name;}

    public void setName(String name) {this.name = name;}

    public String getDate() {return date;}

    public void setDate(String date) {this.date = date;}

    public User getUser() {return user;}

    public void setUser(User user) {this.user = user;}
}