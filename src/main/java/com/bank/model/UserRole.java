package com.bank.model;

import javax.persistence.*;

/**
 * Created by Corina on 02.04.2016.
 */
@Entity
@Table(name="userrole")
public class UserRole {
    @Id
    @GeneratedValue
    @Column(name="id")
    private int id;

    @Column (name="role")
    private String  role;

    private User user;

    public int getId() {return id;}

    public void setId(int id) {this.id = id;}

    public String getRole() {return role;}

    public void setRole(String role) {this.role = role;}

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "name", nullable = false)
    public User getUser() {
        return this.user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}