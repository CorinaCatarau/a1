package com.bank.model;

import javax.persistence.*;

/**
 * Created by Corina on 02.04.2016.
 */
@Entity
@Table(name="bill")
public class Bill {
    @Id
    @GeneratedValue
    @Column(name="id")
    private int id;

    @Column(name="name")
    private String name;

    @Column(name="emissionDay")
    private int emissionDay;

    @Column(name="amount")
    private int amount;

    @ManyToOne
    private Client client;

    public int getId() {return id;}

    public void setId(int id) {this.id = id;}

    public String getName() {return name;}

    public void setName(String name) {this.name = name;}

    public int getEmissionDay() {return emissionDay;}

    public void setEmissionDay(int emissionDay) {this.emissionDay = emissionDay;}

    public int getAmount() {return amount;}

    public void setAmount(int amount) {this.amount = amount;}

    public Client getClient() {return client;}

    public void setClient(Client client) {this.client = client;}
}
