package com.bank.model;


import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


/**
 * Created by Corina on 28.03.2016.
 */
@Entity
@Table(name="user")
@Inheritance(strategy=InheritanceType.TABLE_PER_CLASS)
public class User {

    @Id
    @GeneratedValue
    @Column(name="id")
    private int id;

    @Column(name="name")
    private String name;

    @Column(name="password")
    private String password;

    @Column(name="admin")
    private boolean admin;


    @OneToMany(fetch = FetchType.EAGER,mappedBy = "user")
    private List<Activity> activityList;

    @Column(name="role")
    private String role;

    public int getId() {return id;}

    public void setId(int id) {this.id = id;}

    public String getName() {
            return name;
        }

    public void setName(String name) {
            this.name = name;
        }


    public String getPassword() {return password;}

    public void setPassword(String password) {this.password = password;}

    public boolean isAdmin() {return admin;}

    public void setAdmin(boolean admin) {this.admin = admin;}
    @JsonIgnore
    public List<Activity> getActivityList() {return activityList;}
    @JsonIgnore
    public void setActivityList(List<Activity> activityList) {this.activityList = activityList;}

    public String getRole() {return role;}

    public void setRole(String role) {this.role = role;}
}

