var myApp = angular.module('app');

myApp .controller('TransferCtrl',['$scope','$http','$mdDialog','$mdMedia','$window', function($scope,$http, $mdDialog, $mdMedia,$window){
    $scope.selectedSourceClient={};
    $scope.selectedDestinationClient={};
    $scope.setSource=true;
    $scope.setDestination=true;
    getAccountsForTransfers1=function(id){
        $http({
            method: 'GET',
            url: '/rest/accounts/clients/'+id
        }).then(function successCallback(data) {
            $scope.sAcc=data.data;

        }, function errorCallback(data) {
            console.log("error");
        });
    }

    getAccountsForTransfers2=function(id){
        $http({
            method: 'GET',
            url: '/rest/accounts/clients/'+id
        }).then(function successCallback(data) {
            $scope.dAcc=data.data;

        }, function errorCallback(data) {
            console.log("error");
        });
    }

    $scope.setDestinationClient=function(client){
        $scope.selectedDestinationClient=client;
        getAccountsForTransfers2(client.id);
        $scope.setDestination=false;
    }
    $scope.setSourceClient= function (client) {
        $scope.selectedSourceClient=client;
        getAccountsForTransfers1(client.id);
        $scope.setSource=false;

    }

    $scope.backSource=function(){
        $scope.setSource=true;
    }
    $scope.backDestination=function(){
        $scope.setDestination=true;
    }

    $scope.setDestinationAccount=function(accountDest){
       $scope.destinationAccount=accountDest;
        $scope.transaction.destinationAccountId=accountDest.id;
    }
    $scope.setSourceAccount= function (accountSource) {
        $scope.sourceAccount=accountSource;
        $scope.transaction.sourceAccountId=accountSource.id;



    }

    $scope.showAlertSuccessTransfer = function(ev,amount) {
        $mdDialog.show(
            $mdDialog.alert()
                .parent(angular.element(document.querySelector('#popupContainer')))
                .clickOutsideToClose(true)
                .title('Success')
                .textContent('Transfer of '+amount+' completed')
                .ariaLabel('Alert Dialog Demo')
                .ok('Ok')
                .targetEvent(ev)
        );
    };
    $scope.transferAmountSource = "";
    $scope.transaction={};
    $scope.showAlert = function(ev) {
        $mdDialog.show(
            $mdDialog.alert()
                .parent(angular.element(document.querySelector('#popupContainer')))
                .clickOutsideToClose(true)
                .title('Forbidden')
                .textContent('The amount inserted is invalid.')
                .ariaLabel('Alert Dialog Demo')
                .ok('Got it!')
                .targetEvent(ev)
        );
    };
    $scope.transferOperation=function(event,amount){

        $scope.transaction.amount=parseInt($scope.transaction.amount);
            var newTransfer=angular.toJson($scope.transaction);
            $http({
                method: 'POST',
                url: '/rest/transaction',
                headers: {
                    'Content-Type': 'application/json'
                },
                data: newTransfer
            }).then(function successCallback(data) {
                if(data.data===false){
                    $scope.showAlert(event);
                    console.log("transaction error");


                }else  if(data.data===true){
                    $scope.showAlertSuccessTransfer(event,amount);
                    console.log("transaction succesfull");
                    createActivity("transaction from:"+$scope.transaction.sourceAccountId+" to: "+$scope.transaction.destinationAccountId);

                }
               $scope.setDestination=true;
                $scope.setSource=true;

            }, function errorCallback(data) {

                console.log("error at account create");
            });
        };

    createActivity = function(name) {
        var allDate=new Date();
        var goodDate=allDate.getFullYear() + '-' + ('0' + (allDate.getMonth() + 1)).slice(-2) + '-' + ('0' + allDate.getDate()).slice(-2);
        var  activity={
            "name":name,
            "date":goodDate
        }
        var newActivity=angular.toJson(activity);
        $http({
            method: 'POST',
            url: '/rest/'+$window.localStorage.getItem("userId")+'/activity',
            headers: {
                'Content-Type': 'application/json'
            },
            data: newActivity
        }).then(function successCallback(data) {
            if(data.status===201){
                console.log("activity create succesfull");
            }


        }, function errorCallback(data) {

            console.log("error at activity create");
        });
        ;
    };




}]);