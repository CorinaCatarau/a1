var app = angular.module('app', ['ngMaterial','ngMessages']);
app.directive('toolbar', function() {
    return {
        restrict: 'E'
        ,
        replace: true,
        templateUrl: 'navbar'
    };
});

app.directive('activities', function() {
    return {
        restrict: 'E',
        replace: true,
        templateUrl: 'activities'
    };
});
app.directive('clients', function() {
    return {
        restrict: 'E',
        replace: true,
        templateUrl: 'clients'
    };
});

app.directive('transfers', function() {
    return {
        restrict: 'E',
        replace: true,
        templateUrl: 'transfers'
    };
});

app.directive('showAccounts', function() {
    return {
        restrict: 'E',
        replace: true,
        templateUrl: 'showAccounts'
    };
});

app.directive('destAccounts', function() {
    return {
        restrict: 'E',
        replace: true,
        templateUrl: 'destAccounts'
    };
});

app.directive('bills', function() {
    return {
        restrict: 'E',
        replace: true,
        templateUrl: 'bills'
    };
});



    app .controller('AppCtrl',['$scope','$http','$mdDialog','$mdMedia','$window', function($scope,$http, $mdDialog, $mdMedia,$window){
    $scope.username;
    $scope.activities=[];
    $scope.clients=[];
    $scope.clientsView=false;
    $scope.activitiesView=true;
    $scope.transferView=false;
    $scope.billsView=false;
    $scope.editAccountMode=true;
    $scope.editIndex=-1;
    $scope.newAccount={};
    $scope.newAccount.billAccount=false;
    $scope.editClient=false;
    $scope.editClientIndex=-1;
    $scope.collapsedArea=-1;
    $scope.collapsedItem=-1;

    $scope.notCollpased=false;
    $scope.list=["A","B","C","E"];

    console.log("${username}");
    console.log("<%=username%>");

    $scope.showAlert = function(ev) {
        $mdDialog.show(
            $mdDialog.alert()
                .parent(angular.element(document.querySelector('#popupContainer')))
                .clickOutsideToClose(true)
                .title('Forbidden')
                .textContent('You already have a bill account.')
                .ariaLabel('Alert Dialog Demo')
                .ok('Got it!')
                .targetEvent(ev)
        );
    };
        showInvalidAmount = function(ev) {
            $mdDialog.show(
                $mdDialog.alert()
                    .parent(angular.element(document.querySelector('#popupContainer')))
                    .clickOutsideToClose(true)
                    .title('Forbidden')
                    .textContent('The amount must be > 0.')
                    .ariaLabel('Alert Dialog Demo')
                    .ok('OK')
                    .targetEvent(ev)
            );
        };

    $scope.showAlertSuccess = function(ev) {
        $mdDialog.show(
            $mdDialog.alert()
                .parent(angular.element(document.querySelector('#popupContainer')))
                .clickOutsideToClose(true)
                .title('Success')
                .textContent('Your account has been created')
                .ariaLabel('Alert Dialog Demo')
                .ok('Got it!')
                .targetEvent(ev)
        );
    };


    $scope.getData = function(username) {
        $http({
            method: 'GET',
            url: '/rest/users/username/'+username
        }).then(function successCallback(data) {
            $scope.getActivities(data.data.id);
            $window.localStorage.setItem("userId",data.data.id);
        }, function errorCallback(data) {
            console.log("error");
        });
    };
    $scope.getActivities=function(id){
        $http({
            method: 'GET',
            url: '/rest/activities/users/'+id
        }).then(function successCallback(data) {
            $scope.activities=data.data;
        }, function errorCallback(data) {
            console.log("error");
        });
    };

    getClients=function(){
        $http({
            method: 'GET',
            url: '/rest/clients/'
        }).then(function successCallback(data) {
            $scope.clients=data.data;
            for(var i=0;i<$scope.clients.length;i++){
                getAccounts(data.data[i].id,i);
            }
        }, function errorCallback(data) {
            console.log("error");
        });
    }

    getClientsForTransfers=function(){
        $http({
            method: 'GET',
            url: '/rest/clients/'
        }).then(function successCallback(data) {
            $scope.clientsTransfer=data.data;


        }, function errorCallback(data) {
            console.log("error");
        });
    }

    getClientsForBills=function(){
        $http({
            method: 'GET',
            url: '/rest/clients/'
        }).then(function successCallback(data) {
            $scope.clientsBills=data.data;


        }, function errorCallback(data) {
            console.log("error");
        });
    }
    $scope.deleteAccount=function(account,accounts){
        var index = accounts.indexOf(account);
        var delAccount=account.name;
        $http({
            method: 'DELETE',
            url: '/rest/accounts/'+account.id
        }).then(function successCallback() {
            accounts.splice(index, 1);
            createActivity("delete account: "+delAccount);
            console.log("Delete succesful");

        }, function errorCallback() {
            console.log("error at account delete");
        });

    }

    $scope.deleteClient=function(client,clients){
        var index = clients.indexOf(client);
        var deleted=client.name;
        $http({
            method: 'DELETE',
            url: '/rest/clients/'+client.id
        }).then(function successCallback() {
            clients.splice(index, 1);
            createActivity("delete client: "+deleted);
            console.log("Delete succesful");

        }, function errorCallback() {
            console.log("error at account delete");
        });

    }
    $scope.updateAccount=function(account,event){
        console.log(account);
        var nAccount=angular.fromJson(angular.toJson(account));
        $http({
            method: 'PUT',
            url: '/rest/accounts/'+account.id,
            headers: {
                'Content-Type': 'application/json'
            },
            data: nAccount
        }).then(function successCallback(data) {
            console.log("Update succesfull");
            createActivity("update client: "+data.data.name);
            $scope.editAccountMode=true;
            $scope.showAlertSuccess(event);

        }, function errorCallback(data) {
            if(data.status==400){
                showNotNull(event);
            }else if(data.status==406){
                showInvalidAmount(event);
            }
            console.log("error at account update");
        });

    }

        showInvalidCnp = function(ev) {
            $mdDialog.show(
                $mdDialog.alert()
                    .parent(angular.element(document.querySelector('#popupContainer')))
                    .clickOutsideToClose(true)
                    .title('Invalid')
                    .textContent('The PNC you inserted has an invalid format.')
                    .ariaLabel('Alert Dialog Demo')
                    .ok('Ok')
                    .targetEvent(ev)
            );
        };
        showNotNull = function(ev) {
            $mdDialog.show(
                $mdDialog.alert()
                    .parent(angular.element(document.querySelector('#popupContainer')))
                    .clickOutsideToClose(true)
                    .title('Empty')
                    .textContent('Required fields must not be empty.')
                    .ariaLabel('Alert Dialog Demo')
                    .ok('Ok')
                    .targetEvent(ev)
            );
        };

    $scope.updateClient=function(client,event){
        var nClient=angular.fromJson(angular.toJson(client));
        delete nClient.accounts;
        $http({
            method: 'PUT',
            url: '/rest/clients/'+client.id,
            headers: {
                'Content-Type': 'application/json'
            },
            data: nClient
        }).then(function successCallback(data) {
            console.log("Update succesfull");
            createActivity("update client: "+data.data.name);
            $scope.editClient=false;

        }, function errorCallback(data) {
            if(data.status===409){
                $scope.showExistingCnp(event);
            }else if(data.status===406){
                    showInvalidCnp(event);
            }else if(data.status===400){
                    showNotNull(event);
            }
            console.log("error at account update");
        });

    }
    $scope.editMode=function(index){
        $scope.editAccountMode=false;
        $scope.editIndex=index;
    }
    $scope.editAccount=function(index){
        if((index===$scope.editIndex)&&(!$scope.editAccountMode)){
            return false;
        }
        return true;
    }
    $scope.setEditMode=function(index){
        $scope.editClientIndex=index;
        $scope.editClient=!$scope.editClient;
    }

    $scope.editModeClient=function(index){
        return (($scope.editClientIndex===index)&&$scope.editClient)
    }
    $scope.isCollapsed=function(index,i){
        $scope.collapsedArea=i;
        $scope.collapsedItem=index;
        $scope.notCollpased=true;
    }
    $scope.collapsedAreaFunction=function(index,i){
        return((index===$scope.collapsedItem)&&(i===$scope.collapsedArea)&& $scope.notCollpased)
    }

    getAccounts=function(id,i){
        $http({
            method: 'GET',
            url: '/rest/accounts/clients/'+id
        }).then(function successCallback(data) {
            $scope.clients[i].accounts=data.data;

        }, function errorCallback(data) {
            console.log("error");
        });
    }
    $scope.switchView=function(view){
        if(view==="clients"){
            $scope.clientsView=true;
            $scope.activitiesView=false;
            $scope.transferView=false;
            $scope.billsView=false;
            getClients();
        }else if(view==="activities"){
            $scope.clientsView=false;
            $scope.activitiesView=true;
            $scope.transferView=false;
            $scope.billsView=false;
        }else if (view==="transfers"){
            $scope.clientsView=false;
            $scope.activitiesView=false;
            $scope.transferView=true;
            $scope.billsView=false;
            getClientsForTransfers();
        }if (view==="bills"){
            $scope.clientsView=false;
            $scope.activitiesView=false;
            $scope.transferView=false;
            $scope.billsView=true;
            getClientsForBills();
        }

    }
    $scope.createAccount = function(client,newAccount,accounts,event) {
        console.log(newAccount);
        var nAccount=angular.fromJson(angular.toJson(newAccount));
        $http({
            method: 'POST',
            url: '/rest/'+client.id+'/account/',
            headers: {
                'Content-Type': 'application/json'
            },
            data: nAccount
        }).then(function successCallback(data) {
            accounts.push(data.data);
            if(data.status===201){
                $scope.showAlertSuccess(event);
                createActivity("create account for client "+data.data.name);

            }
            $scope.newAccount.name="";
            $scope.newAccount.amount="";
            console.log("create succesfull");

        }, function errorCallback(data) {
            if(data.status===403){
                $scope.showAlert(event);
            }else if (data.status==400){
                showNotNull(event);
            }else if(data.status==406){
                showInvalidAmount(event);
            }
            console.log("error at account create");
        });
        ;
    };
        $scope.showExistingCnp = function(ev) {
            $mdDialog.show(
                $mdDialog.alert()
                    .parent(angular.element(document.querySelector('#popupContainer')))
                    .clickOutsideToClose(true)
                    .title('Conflict')
                    .textContent('There already exists a client with this PNC.')
                    .ariaLabel('Alert Dialog Demo')
                    .ok('Ok')
                    .targetEvent(ev)
            );
        };

    $scope.createClient = function(newClient,event) {
        var clientNew=angular.fromJson(angular.toJson(newClient));
        $http({
            method: 'POST',
            url: '/rest/client/',
            headers: {
                'Content-Type': 'application/json'
            },
            data: clientNew
        }).then(function successCallback(data) {
            if(data.status===201){
                $scope.showAlertSuccess(event);
                createActivity("create user");
            }
            $scope.isCollapsed=true;
            $scope.clients.push(data.data);
            console.log("create succesfull");

        }, function errorCallback(data) {
            if(data.status===409){
                $scope.showExistingCnp(event);
            }
            console.log("error at client create");
        });
        ;
    };

        createActivity = function(name) {
            var allDate=new Date();
            var goodDate=allDate.getFullYear() + '-' + ('0' + (allDate.getMonth() + 1)).slice(-2) + '-' + ('0' + allDate.getDate()).slice(-2);
            var  activity={
                "name":name,
                "date":goodDate
            }
            var newActivity=angular.toJson(activity);
            $http({
                method: 'POST',
                url: '/rest/'+$window.localStorage.getItem("userId")+'/activity',
                headers: {
                    'Content-Type': 'application/json'
                },
                data: newActivity
            }).then(function successCallback(data) {
                if(data.status===201){
                    console.log("activity create succesfull");
                }


            }, function errorCallback(data) {

                console.log("error at activity create");
            });
            ;
        };



}]);