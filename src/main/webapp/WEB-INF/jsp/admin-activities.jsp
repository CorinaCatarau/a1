<div class="container">
    <div class="page-header">
        <h1 id="timeline">User Activities</h1>
    </div>
    <md-input-container>
        <label>User</label>
        <md-select placeholder="User" ng-model="userSelected" ng-change="onchange(userSelected)">
            <md-option ng-repeat="user in users" ng-value="{{user}}" >
                {{user.name}}
            </md-option>
        </md-select>
    </md-input-container>
    <div layout="row">
        <h4>Start Date</h4>
        <md-datepicker ng-model="myStartDate" md-placeholder="Enter date" md-max-date="maxDate"></md-datepicker>
        <h4>End Date</h4>
        <md-datepicker ng-model="myEndDate" md-placeholder="Enter date" md-max-date="maxDate" md-min-date="minDate"></md-datepicker>
    </div>
    <md-button class="lavender" ng-click="getActivities(userSelected,myStartDate,myEndDate)">SHOW</md-button>
    <ul class="timeline" ng-if="showActivities">
        <li ng-repeat="item in activities" ng-class-odd="" ng-class-even="'timeline-inverted'">
            <div class="timeline-badge"><i class="material-icons">playlist_add_check</i></div>
            <div class="timeline-panel">
                <div class="timeline-heading">
                    <h4 class="timeline-title">Activity</h4>
                    <p><small class="text-muted"><i class="glyphicon glyphicon-time"></i>Date:{{item.date}}</small></p>
                </div>
                <div class="timeline-body">
                    <p>{{item.name}}</p>
                </div>
            </div>
        </li>

    </ul>
</div>
