<div flex-gt-sm="50" flex="">

        <md-toolbar layout="row" class="md-hue-3">
            <div class="md-toolbar-tools">
                <span>{{selectedDestinationClient.name}}</span>
            </div>
        </md-toolbar>

        <md-content >

            <md-list  flex="">
                <md-list-item class="lavender" class="md-4-line"  ng-click="null" ng-repeat="dest in dAcc"  >
                    <div class="md-list-item-text" layout="column">
                        <h3>{{ dest.name}}</h3>
                        <h4>{{ dest.amount }}</h4>
                        <p>{{ dest.billAccount }}</p>
                    </div>
                    <md-divider></md-divider>
                    <md-checkbox class="md-secondary" ng-model="checkedDestAcc" ng-change="setDestinationAccount(dest)"></md-checkbox>
                </md-list-item>


            </md-list>
        </md-content>

    </div>

