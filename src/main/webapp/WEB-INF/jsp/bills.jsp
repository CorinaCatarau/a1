<md-content ng-controller="BillsCtrl">
    <h1>Bill Payment</h1>
    <md-input-container>
        <label>Client</label>
        <md-select placeholder="Client" ng-model="clientSelected" ng-change="onchange(clientSelected)">
            <md-option ng-repeat="client in clientsBills" ng-value="{{client}}" >
                {{client.name}}
            </md-option>
        </md-select>
    </md-input-container>

    <md-button ng-show="showBills" class="lavender" ng-model="collapsed" ng-click="collapsed=!collapsed">ADD BILL</md-button>
    <div ng-show="collapsed">
        <form name="billForm" ng-submit="createBill(newBill,$event,bills)">

            <md-input-container>
                <label>Type</label>
                <md-select placeholder="Bill type" ng-model="newBill.name" >
                    <md-option  >EON</md-option>
                    <md-option  >ELECTRICA</md-option>
                    <md-option  >UPC</md-option>
                    <md-option  >DIGI</md-option>

                </md-select>
            </md-input-container>

            <md-input-container>
                <label>Emission Day</label>
                <md-select ng-model="newBill.emissionDay">
                    <md-option ng-repeat="day in days" value="{{day}}">
                        {{day}}
                    </md-option>
                </md-select>
            </md-input-container>

            <md-input-container class="md-block">
                <label>Amount</label>
                <input  name="newBillAmount" ng-model="newBill.amount" required="">
                <div ng-messages="billForm.newBill.amount.$error">
                    <div ng-message="required">This is required.</div>
                </div>
            </md-input-container>
            <md-button type="submit" id="submit" class="lavender" ng-disabled="billForm.$invalid">Submit</md-button>

        </form>
        </div>

    <md-content ng-repeat="bill in bills">
        <md-list flex="">
            <md-list-item class="lightBlue" class="md-4-line"  ng-click="null" ng-if="editModeBill($index)">
                <img ng-src="" class="md-avatar" alt="">
                <div class="md-list-item-text" layout="column">
                    <md-input-container>
                        <label>Type</label>
                        <md-select placeholder="Bill type" ng-model="bill.name" >
                            <md-option  >EON</md-option>
                            <md-option  >ELECTRICA</md-option>
                            <md-option  >UPC</md-option>
                            <md-option  >DIGI</md-option>

                        </md-select>
                    </md-input-container>
                    <md-input-container class="md-block">
                        <label>Pay day</label>
                        <input  required="" md-no-asterisk="" name="payDay" ng-model="bill.emissionDay">
                        <div ng-messages="bill.emissionDay.$error">
                            <div ng-message="required">This is required.</div>
                        </div>
                    </md-input-container>
                    <md-input-container class="md-block">
                        <label>Amount</label>
                        <input  required="" md-no-asterisk="" name="billAmount" ng-model="bill.amount">
                        <div ng-messages="bill.amount.$error">
                            <div ng-message="required">This is required.</div>
                        </div>
                    </md-input-container>
                </div>
                <div layout="row" flex>
                    <md-button >
                        <i class="material-icons" ng-click="updateBill(bill)">arrow_forward</i>
                    </md-button>

                </div>
            </md-list-item>
            <md-card  ng-if="!editModeBill($index)">
                <md-card-title>
                    <md-card-title-text>
                        <span class="md-headline">{{ bill.name}}</span>
                        <span class="md-subhead">Emission Day:{{ bill.emissionDay }}</span>
                        <span class="md-subhead">{{ bill.amount }}</span>
                    </md-card-title-text>
                    <md-card-title-media>
                        <div class="md-media-sm card-media"></div>
                    </md-card-title-media>
                </md-card-title>
                <md-card-actions layout="row" layout-align="end center">
                    <md-button class="lightBlue" ng-click="payBill(bill,event)" >Pay Bill</md-button>
                    <md-button class="md-fab" >
                        <i class="material-icons"  class="md-secondary md-hue-3" ng-click="setEditModeBill($index)">mode_edit</i>
                    </md-button>
                    <md-button class="md-fab">
                        <i class="material-icons"  class="md-secondary" ng-click="deleteBill(bill,bills)">delete_forever</i>
                    </md-button>
                </md-card-actions>
            </md-card>


        </md-list>
        </md-content>
</md-content>