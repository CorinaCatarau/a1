<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<md-toolbar ng-app="app">
    <div class="md-toolbar-tools">
        <md-button ng-click="switchView('activities')">
            Hello
        </md-button>
        <md-button aria-label="comment" class="md-icon-button" ng-click="switchView('clients')">
            <i class="material-icons" ng-click="switchView('clients')">face</i>
        </md-button>
        <md-button aria-label="label" class="md-icon-button" ng-click="switchView('transfers')">
            <i class="material-icons" ng-click="switchView('transfers')">compare_arrows</i>
        </md-button>
        <md-button aria-label="photo" class="md-icon-button" ng-click="switchView('bills')">
            <i class="material-icons" ng-click="switchView('bills')">attach_money</i>
        </md-button>
        <a href="<c:url value="/logout" />">Logout</a>

    </div>
</md-toolbar>
